#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <list>
#include "DispatcherQueue.hpp"
#ifdef WIN32
#include <winSock2.h>
#include <conio.h>
#include <windows.h>
#include <ws2tcpip.h>
#else
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>

#define stricmp strcasecmp
#endif

using namespace std;

int TcpProtocol = 1;

void InitSockets();
void RunDispatcher();
void SetServerAddress(sockaddr_in *sockAddr, int port);
void SetNonBlock(int hSocket);
void Closesocket(int hSocket);
void CreateSockets();
void Itoa(int number, char* buffer);
void ShowHostInterfaces();
void SetConsoleEncoding();
timeval SetTimeout(int seconds = 1, int usec = 500000);
int CreateSocketsSet(fd_set* sockSet);
void SendProcessInfo(int hSocket);
int ReceiveProcessPid();

int main(int argc, char* argv[])
{
	SetConsoleEncoding();
	printf("Enter '1' to select TCP protocol or '2' to select UDP protocol\n");
	scanf("%d", &TcpProtocol);
	InitSockets();
}

void InitSockets()
{
	#ifdef WIN32
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 0), &wsaData) == 0)
	{
		if(LOBYTE(&wsaData, wVersion) >= 2)
		{
			ShowHostInterfaces();
			RunDispatcher();
		}
		else printf("Version error\n");
		WSACleanup();
	}
	else printf("Initialize error\n");
	getch();
	#else
	RunDispatcher();
	#endif
}

void RunDispatcher()
{
	HANDLE hEvent = CreateEvent(NULL, true, false, "myEvent");
	ResetEvent(hEvent);
	DispatcherQueue::getInstance()->hEvent = hEvent;

	CreateSockets();

	fd_set sockSet;
	while(1)
	{
		int request = 0;
		timeval timeout = SetTimeout(3);
		int highestSocket = CreateSocketsSet(&sockSet);
		if(select(highestSocket + 1, &sockSet, NULL, NULL, &timeout) != 0)						//new request
		{
			list<ServiceInfo*>::iterator i = DispatcherQueue::getInstance()->queue->begin();
			for(i; i != DispatcherQueue::getInstance()->queue->end(); i++)
			{
				if(!(*i)->actionState)
				{
					if(FD_ISSET((*i)->hSocket, &sockSet))									  //find socket
					{
						request = (*i)->hSocket;
						FD_CLR((*i)->hSocket, &sockSet);
						break;
					}
				}
			}

			STARTUPINFO si;
			ZeroMemory(&si, sizeof(STARTUPINFO));
			si.cb = sizeof(STARTUPINFO);
			PROCESS_INFORMATION procInfo;
			ZeroMemory(&procInfo, sizeof(PROCESS_INFORMATION));								//create new process
			CreateProcess("SimpleServer.exe", NULL, NULL, NULL, false, CREATE_NEW_CONSOLE, NULL, NULL, &si, &procInfo);
			(*i)->actionState = true;
			(*i)->windowInfo = si;
			(*i)->procInfo = procInfo;
			SendProcessInfo((*i)->hSocket);	
		}

		list<ServiceInfo*>::iterator i = DispatcherQueue::getInstance()->queue->begin();
		for(i; i != DispatcherQueue::getInstance()->queue->end(); i++)
		{
			if((*i)->actionState)
			{
				int result = WaitForSingleObject((*i)->procInfo.hThread, 0);
				if(result == WAIT_OBJECT_0)
				{
					(*i)->actionState = false;
					CloseHandle((*i)->procInfo.hThread);
					CloseHandle((*i)->procInfo.hProcess);
				}
			}
		}
	}
}

void CreateSockets()
{
	int portCount = 0, port = 0;
	printf("Enter service first port: ");
	scanf("%d", &port);
	printf("Enter number of ports: ");
	scanf("%d", &portCount);

	for(int i = 0; i < portCount; i++)
	{
		int hSocket = 0;
		sockaddr_in serverAddress = {0};
		if(TcpProtocol == 1)
		{
			if((hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
			{
				printf("Initialize socket error\n");
				return;
			}
			SetNonBlock(hSocket);
		}
		else
		{
			if((hSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			{
				printf("Initialize socket error\n");
				return;
			}
		}
		DispatcherQueue::getInstance()->queue->push_back(new ServiceInfo(hSocket, port));
		SetServerAddress(&serverAddress, port);

		if(bind(hSocket, (sockaddr*)(&serverAddress), sizeof(sockaddr)) != 0)
		{
			printf("Binding error\n");
			return;
		}
		if(TcpProtocol == 1)
		{
			if(listen(hSocket, SOMAXCONN) != 0)
			{
				printf("Listen error\n");
				return;
			}
		}
		port++;
	}
}

void SetServerAddress(sockaddr_in *sockAddr, int port)
{
	sockAddr->sin_family = AF_INET;
	sockAddr->sin_addr.s_addr = INADDR_ANY;
	char sPort[25];
	Itoa(port, sPort);
	if(sPort[0] > 47 && sPort[0] < 58)								//numbers
		sockAddr->sin_port = htons(atoi((const char*)sPort));
	else															//name
	{
		servent* serv = getservbyname(sPort, NULL);
		if(serv == NULL)
		{
			printf("Entered unknown name. Use default port 7777\n");
			return;
		}
		sockAddr->sin_port = serv->s_port;
		printf("port : %d\n", htons(serv->s_port));
	}
}

void SetNonBlock(int hSocket)
{
	#ifdef WIN32
	DWORD dw = true;
	ioctlsocket(hSocket, FIONBIO, &dw);
	#else
	fcntl(hSocket, F_SETFL, O_NONBLOCK);
	#endif
}

void CloseSocket(int hSocket)
{
	#ifdef WIN32
	closesocket(hSocket);
	#else 
	close(hSocket);
	#endif
}

void Itoa(int number, char* buffer)
{
	#ifdef WIN32
	itoa(number, (char*)buffer, 10);
	#else
	sprintf((char*)buffer, "%d", number);
	#endif
}

void ShowHostInterfaces()
{
	hostent* host;
	in_addr addr;
	int i = 0;
	addr.s_addr = inet_addr("127.0.0.1");
	host = gethostbyaddr((char*)&addr, 4, AF_INET);
	host = gethostbyname(host->h_name);
	printf("%s\n", host->h_name);
	while(host->h_addr_list[i] != 0)
	{
		addr.s_addr = *(u_long*)host->h_addr_list[i++];
		printf("Address #%d %s\n", i, inet_ntoa(addr));
	}
	host = NULL;
	delete host;
}

void SetConsoleEncoding()
{
	#ifdef WIN32
	SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
	#endif
}

timeval SetTimeout(int seconds, int usec)
{
	timeval timeout;
	timeout.tv_sec = seconds;
	timeout.tv_usec = usec;
	return timeout;
}

int CreateSocketsSet(fd_set* sockSet)
{
	FD_ZERO(sockSet);
	int highestSocket = 0;
	list<ServiceInfo*>::iterator i = DispatcherQueue::getInstance()->queue->begin();
	for(i; i != DispatcherQueue::getInstance()->queue->end(); i++)
	{
		if(!(*i)->actionState)
		{
			if((*i)->hSocket > highestSocket)
				highestSocket = (*i)->hSocket;
			FD_SET((*i)->hSocket, sockSet);
		}
	}
	return highestSocket;
}

int ReceiveProcessPid()
{
	char* pipeName  = "\\\\.\\pipe\\MyPipe";
	HANDLE pipe = CreateNamedPipe(pipeName, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
		5, 1024, 1024, 5000, NULL);
	DispatcherQueue::getInstance()->namedPipe = pipe;
	
	SetEvent(DispatcherQueue::getInstance()->hEvent);
	Sleep(100);
	WaitForSingleObject(DispatcherQueue::getInstance()->hEvent, INFINITE);
	int procPid = 0;
	DWORD read = 0;
	ReadFile(DispatcherQueue::getInstance()->namedPipe, &procPid, sizeof(int), &read, NULL);
	return procPid;
}

void SendProcessInfo(int hSocket)
{
	int targetPid = ReceiveProcessPid();
	WSAPROTOCOL_INFO protocolInfo;
	int size = sizeof(protocolInfo);
	ZeroMemory(&protocolInfo, size);
	WSADuplicateSocket(hSocket, targetPid, &protocolInfo);
	DWORD written = 0;
	WriteFile(DispatcherQueue::getInstance()->namedPipe, &protocolInfo, size, &written, NULL);
	SetEvent(DispatcherQueue::getInstance()->hEvent);
	DisconnectNamedPipe(DispatcherQueue::getInstance()->namedPipe);
	CloseHandle(DispatcherQueue::getInstance()->namedPipe);
}