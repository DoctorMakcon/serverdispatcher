#include "DispatcherQueue.hpp"

ServiceInfo::ServiceInfo(int socket, int port)
{
	hSocket = socket;
	this->port = port;
	actionState = false;
}

DispatcherQueue::DispatcherQueue()
{
	queue = new list<ServiceInfo*>();
}

DispatcherQueue* DispatcherQueue::getInstance()
{
	if(!instance)
		instance = new DispatcherQueue();
	return instance;
}

DispatcherQueue::~DispatcherQueue()
{
	list<ServiceInfo*>::iterator i = queue->begin();
	for(i; i != queue->end(); i++)
		queue->remove(*i);
	queue = NULL;
	delete queue;
}

list<ServiceInfo*>::iterator DispatcherQueue::FindInfo(int socket)
{
	list<ServiceInfo*>::iterator i = queue->begin();
	for(i; i != queue->end(); i++)
	{
		if((*i)->hSocket == socket)
			return i;
	}
	return i;
}

DispatcherQueue* DispatcherQueue::instance = NULL;
