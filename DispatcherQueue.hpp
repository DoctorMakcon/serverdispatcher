#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <list>
#ifdef WIN32
#include <winSock2.h>
#else
#include <sys/types.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

//hello kitty hello!
using namespace std;

class ServiceInfo
{
public:
	int hSocket;
	int port;
	bool actionState;
	STARTUPINFO windowInfo;
	PROCESS_INFORMATION procInfo;

	ServiceInfo(int socket, int port);
	~ServiceInfo(){};
};

class DispatcherQueue
{
public :
	HANDLE namedPipe;
	HANDLE hEvent;
	list<ServiceInfo*>* queue;
	static DispatcherQueue* getInstance();
	list<ServiceInfo*>::iterator FindInfo(int hSocket);
	~DispatcherQueue();

private:
	DispatcherQueue();
	static DispatcherQueue* instance;
};